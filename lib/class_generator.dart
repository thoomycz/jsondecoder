import 'dart:mirrors';
import 'annotation.dart';
import 'dart:io';

const ST_SEARCH = 0;
const ST_BEGIN = 1;
const ST_CLASS = 3;

class ClassProperty {
  String _name;
  String jName;
  set name(String v) {_name = v; jName = _name.substring(0,1).toUpperCase() + _name.substring(1);}
  String get name => _name;
  String type;
  String fullType;
  String typeArgument;
  bool map = false;
  bool list = false;
  bool decodable = false;
  bool isDynamic = false;
  bool getter = false;
  bool setter = false;
  bool static = false;
  bool fromJson = false;
  bool staticka = false;

  String fromJsonAsignment() {
    String ret;

    if (name == "mapa") {
      print("decodable: ${decodable}");
      print("from json: ${fromJson}");
      print("is dynamic: ${isDynamic}");
    }

    if ((!decodable && !fromJson) || isDynamic) {
      if (map) {
        ret = '$name = (m["$jName"] as Map).map((k, v) { return new MapEntry(k, v as ${typeArgument});});';
      } else if  (isDynamic) {
        ret = '$name = m["$jName"];';
      } else {
        if (type == "DateTime") {
          ret =
          '$name = m["$jName"] != null ? DateTime.parse(m["$jName"]).toLocal() : null;';
        } else if (list) {
          ret =
          '$name = (m["$jName"] as List)?.cast<$typeArgument>()?.toList();';
        } else {
          ret = '$name = m["$jName"];';
        }
      }
    } else {
      if (map) {
        ret = '$name = new $typeArgument().mapFromJson(m["$jName"]);';
      } else if (list) {
        ret = '$name = new $typeArgument().listFromJson(m["$jName"]);';
      } else {
        if (fromJson) {
          if (staticka) {
            ret = '$name = $type.fromJson(m["$jName"]);';
          } else {
            ret = '$name = new $type().fromJson(m["$jName"]);';
          }
        } else {
          ret = '$name = new $type()..fromJson(m["$jName"]);';
        }
      }
    }

    return ret;
  }
}

class ClassData {
  String name;
  String source = "";
  String newSource = "";
  List<ClassProperty> properties = [];

  bool getSource(String src) {
    int state = ST_BEGIN;
    int brack = 0;
    int c = 0;
    int classBegin;
    int classEnd;
    String lines = src;
    RegExp regExDecodable = new RegExp(r"\n(\w*\s)?class\s+" + name + r'\s+');
    while (c < lines.length) {
      if (c < lines.length-2 && lines.substring(c, c + 2) == "//") {
        while (lines.substring(c, c + 1) != "\n") {
          c++;
        }
      }
      if (c < lines.length-2 && lines.substring(c, c + 2) == "/*") {
        while (lines.substring(c, c + 2) != "*/") {
          c++;
        }
      }
      if (state == ST_BEGIN) {
        if (lines.substring(c).startsWith(regExDecodable)) {
          state = ST_CLASS;
          classBegin = c;
        }
      }
      if (state == ST_CLASS) {
        if (lines.substring(c, c + 1) == "{") {
          brack++;
        }
        if (lines.substring(c, c + 1) == "}") {
          brack--;
          if (brack == 0) {
            classEnd = c;
            source = lines.substring(classBegin, classEnd);
            generateClass();
            return true;
          }
        }
      }
      c++;
    }
    return false;
  }
  void generateClass() {

    String abstractName = '_\$${name}Mixin';

    String out = "abstract class $abstractName { \n\n";

    properties.forEach((p) {
      out += '  ${p.fullType} ${p.name};\n';
    });
    out += "\n";

      out += '''  $name newInstance() {
    return new $name();
  }\n''';

    out += '  Map<String, dynamic> toJson() {\n    return <String, dynamic>{\n';
    properties.forEach((p) {
      if (!p.setter && !p.static) {
        if (p.type == "DateTime") {
          out += '      "${p.jName}": ${p.name} != null ? ${p
              .name}.toUtc().toIso8601String() : null,\n';
        } else if (p.decodable) {
          if (p.list) {
            out += '      "${p.jName}": ${p.name} != null ? new ${p
                .typeArgument}().listToJson(${p.name}) : null,\n';
          } else if (p.map) {
            out += '      "${p.jName}": ${p.name} != null ? new ${p
                .typeArgument}().mapToJson(${p.name}) : null,\n';
          } else {
            out += '      "${p.jName}": ${p.name} != null ? ${p
                .name}.toJson() : null,\n';
          }
        } else {
          out += '      "${p.jName}": ${p.name},\n';
        }
      }
    });

    out += '    };\n  }\n';

    out += '  $name fromJson(Map<String, dynamic> m) {\n    if (m != null) {\n';
    properties.forEach((ClassProperty pd) {
      if (!pd.static && !pd.getter) {
        String s = pd.fromJsonAsignment();
        if (s != null) {
          out += "      " + s + "\n";
        }
      }
    });

    out += '      return this;\n    }\n    return null;\n  }\n';

/*    out += '  void fromJsonString(String json) {\n    Map<String, dynamic> m = JSON.decode(json);\n    fromJson(m);\n  }\n';

    out += '''
  static List<${name}> listFromJson(List<dynamic> json) {
    List<${name}> l = [];
    json.forEach((v) {
      l.add(new ${name}()..fromJson(v));
    });
    return l;
  }
  static List<${name}> listFromJsonString(String json) {
    List<dynamic> jsond = JSON.decode(json);
    return listFromJson(jsond);
  }
''';

//    out += '}\n\n';
*/


//    newSource = source.replaceFirst("JsonDecodableMethod", "JsonDecodable<$name>") + "\n" + out;

      out += "\n}\n\n";
      newSource = out;
  }
}

class JsonDecoderGenerator {
  String file;
  String sourceCode;

  List<ClassData> getClasses() {
    List<ClassData> retList = [];
    print("Reading file: $file");
    MirrorSystem cm = currentMirrorSystem();
    LibraryMirror l = cm.libraries[new Uri.file(file)];
//    ClassMirror jsonDecodable = ;
    ClassMirror jsonDecodable = reflectClass(JsonDecodableMethod);
    if (l != null) {
      l.declarations.forEach((s, DeclarationMirror c) {
        if (c is ClassMirror) {
//          var c = c1;
          if (c.isSubtypeOf(jsonDecodable)) {
            ClassData cd = new ClassData();
            cd.name = MirrorSystem.getName(c.simpleName);
            c.declarations.forEach((ps, DeclarationMirror prop) {
              if (prop is VariableMirror) {
                ClassProperty cp = new ClassProperty();
                cp.name = MirrorSystem.getName(prop.simpleName);
                cp.type = MirrorSystem.getName(prop.type.simpleName);
                cp.fullType = prop.type.reflectedType.toString();
                cp.map = cp.type == "Map";
                cp.list = cp.type == "List";
                cp.isDynamic = cp.type == "dynamic";

//              bool hasFromJson = false;
                if (prop.type is ClassMirror) {
//                print((prop.type as ClassMirror).declarations);
                  cp.fromJson =
                      (prop.type as ClassMirror).declarations.containsKey(
                          new Symbol("fromJson"));
                  if (cp.fromJson) {
                    var fromJsonM = (prop.type as ClassMirror)
                        .declarations[new Symbol("fromJson")];
                    cp.staticka = (fromJsonM as MethodMirror).isStatic;
                  }
                }
//              if (prop.type.isSubtypeOf(reflectClass(Class))) {
//
//                ClassMirror v = reflectClass(prop.type);
//                hasFromJson = v.declarations.containsKey("fromJson");
//              }
                cp.decodable = prop.type.isSubtypeOf(jsonDecodable);
                cp.static = prop.isStatic;
                if (cp.map && prop.type.typeArguments.length == 2) {
                  cp.decodable =
                      MirrorSystem.getName(prop.type.typeArguments[1].simpleName) != "dynamic" && prop.type.typeArguments[1].isSubtypeOf(jsonDecodable);
                  cp.typeArgument = MirrorSystem.getName(
                      prop.type.typeArguments[1].simpleName);
                }
                if (cp.list && prop.type.typeArguments.length == 1) {
                  cp.decodable =
                      prop.type.typeArguments[0].isSubtypeOf(jsonDecodable);
                  cp.typeArgument = MirrorSystem.getName(
                      prop.type.typeArguments[0].simpleName);
                }
                cd.properties.add(cp);
              } else
              if (prop is MethodMirror && (prop.isGetter || prop.isSetter)) {
                ClassProperty cp = new ClassProperty();
                cp.getter = prop.isGetter;
                cp.setter = prop.isSetter;
                cp.static = prop.isStatic;
                if (cp.setter) {
                  cp.name = MirrorSystem.getName(prop.simpleName).replaceAll("=", "");
                } else {
                  cp.name = MirrorSystem.getName(prop.simpleName);
                }
                TypeMirror tm;
                if (cp.getter) {
                  tm = prop.returnType;
                } else {
                  tm = prop.parameters[0].type;
                }
                print(tm.simpleName);
                cp.type = MirrorSystem.getName(tm.simpleName);
                cp.fullType = tm.reflectedType.toString();
                cp.map = cp.type == "Map";
                cp.list = cp.type == "List";
                cp.decodable = tm.isSubtypeOf(jsonDecodable);
                if (cp.map && tm.typeArguments.length == 2) {
                  cp.decodable = tm.typeArguments[1].isSubtypeOf(jsonDecodable);
                }
                if (cp.list && tm.typeArguments.length == 1) {
                  cp.decodable = tm.typeArguments[0].isSubtypeOf(jsonDecodable);
                }
                cd.properties.add(cp);
              }
            });
            if (cd.getSource(sourceCode)) {
              retList.add(cd);
            } else {
              print("source code for ${cd.name} not found");
            }
          }
        }
      });
    } else {
      print("library not found by mirror system");
    }
    return retList;
  }

  String getSourceCode() {
    print(file);
    return new File(file).readAsStringSync();
  }

  void run() {
    List<ClassData> classes = getClasses();

    String out = '';

    out += "part of '" + file.split("/").last + "';\n\n";

    classes.forEach((ClassData c) {
        out += c.newSource;
//      sourceCode = sourceCode.replaceFirst(c.source, c.newSource);

    });
//    sourceCode = sourceCode.replaceFirst("import 'package:jsondec_gen/annotation.dart';", "import 'package:jsondec_gen/annotation.dart';\nimport 'dart:convert';");


//    print(out);

    new File(file.replaceRange(file.lastIndexOf(".dart"), file.length, "_gen.dart")).writeAsStringSync(out);
  }

  JsonDecoderGenerator(this.file) {
    sourceCode = getSourceCode();
  }
}