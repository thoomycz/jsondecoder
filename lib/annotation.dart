import 'dart:convert';

const Jexp jexp = const Jexp();
class Jexp{
  const Jexp();
}

abstract class JsonDecodableToJson {
  Map<String, dynamic> toJson();
}

abstract class JsonDecodableFromJsonString {
  void fromJsonString(String jsonString);
}

abstract class JsonDecodableMethod {}

abstract class JsonDecodable<E> extends JsonDecodableMethod implements
    JsonDecodableToJson,
    JsonDecodableFromJsonString
{

  E fromJson(Map<String, dynamic> m);
  Map<String, dynamic> toJson();

  JsonDecodable<E> newInstance();

  List<E> listFromJson(List<dynamic> json) {
    List<E> l = [];
    if (json != null) {
      json.forEach((v) {
        JsonDecodable<E> i = newInstance();
        l.add(i.fromJson(v));
      });
    }
    return l;
  }

  List<dynamic> listToJson(List<JsonDecodable> o) {
    List<dynamic> l = [];
    o.forEach((JsonDecodable v) {
      l.add(v.toJson());
    });
    return l;
  }

  Map<String, dynamic> mapToJson(Map<String, JsonDecodable> o) {
    Map<String, dynamic> l = {};
    o.forEach((s, JsonDecodable v) {
      l[s] = v.toJson();
    });
    return l;
  }

  Map<String, E> mapFromJson(Map<String, dynamic> json) {
    Map<String, E> l = {};
    if (json != null) {
      json.forEach((k, v) {
        JsonDecodable<E> i = newInstance();
        l[k] = i.fromJson(v);
      });
    }
    return l;
  }

  List<E> listFromJsonString(String jsonString) {
    List<dynamic> jsond = json.decode(jsonString);
    return listFromJson(jsond);
  }

  Map<String, E> mapFromJsonString(String jsonString) {
    Map<String, dynamic> jsond = json.decode(jsonString);
    return mapFromJson(jsond);
  }

  void fromJsonString(String jsonString) {
    Map<String, dynamic> m = json.decode(jsonString);
    fromJson(m);
  }

  String toJsonString() {
    return json.encode(toJson());
  }
}