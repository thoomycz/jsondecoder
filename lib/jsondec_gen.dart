library jsondec_gen;

import 'dart:io';

//import 'annotation.dart';

const ST_SEARCH = 0;
const ST_BEGIN = 1;
const ST_ANNOT = 2;
const ST_CLASS = 3;

class JsonPropertyData {
  String name;
  String type;
}
//todo: U List a Map volat funkce fromList a fromMap
class JsonClassData {
  List<JsonPropertyData> properties;
  String name;
  String source;

  String toString() {
    String out = source.replaceFirst("{", " extends JsonDecodable {").replaceAll("@JsonDecodable()", "").replaceAll("@jexp ", "");
    out += '  Map<String, dynamic> toJson() {\n    return <String, dynamic>{\n';
    properties.forEach((p) {
      out += '      "${p.name}": ${p.name},\n';
    });

    out += '    };\n  }\n';

    out += '  void fromJson(Map<String, dynamic> m) {\n';
    properties.forEach((pd) {
      String p = pd.name;
      if (pd.type == 'DateTime') {
        out +=
        '    $p = DateTime.parse(m["$p"]);\n';
      } else {
        out +=
        '    if (($p as dynamic) is JsonDecodable) {\n      ($p as dynamic).fromJson(m["$p"]);\n    } else {\n      $p = m["$p"];\n    }\n';
      }
    });
    out += '  }\n';

    out += '  void fromJsonString(String json) {\n    Map<String, dynamic> m = JSON.decode(json);\n    fromJson(m);\n  }\n';

    out += '''
  static List<${this.name}> listFromJson(List<dynamic> json) {
    List<${this.name}> l = [];
    json.forEach((v) {
      l.add(new ${this.name}()..fromJson(v));
    });
    return l;
  }
  static List<${this.name}> listFromJsonString(String json) {
    List<dynamic> jsond = JSON.decode(json);
    return listFromJson(jsond);
  }
''';

    out += '}\n\n';

    return out;
  }
  JsonClassData(this.name, this.source, this.properties);
}

class JsonDecoder {
  String fn;

  void generate() {
    List<JsonClassData> classes = read(fn);

    String out = "import 'dart:convert';\nimport 'package:jsondec_gen/jsondec_gen.dart';\n\n";
    out += classes.join("\n\n");

    new File(fn.replaceRange(fn.lastIndexOf(".dart"), fn.length, ".g.dart")).writeAsStringSync(out);

  }

  List<JsonClassData> read(String fn) {
    List<JsonClassData> classes = [];
    int state = ST_BEGIN;
    int brack = 0;
    int c = 0;
    int classBegin;
    int classEnd;
    List<JsonPropertyData> properties = [];
    String lines = new File(fn).readAsStringSync();
    RegExp regExClassName = new RegExp(r'\n(\w*\s)?class\s+([\w_]+)\b');
    RegExp regExDecodable = new RegExp(r'(\n|\s)@JsonDecodable\(\)\s*\n(\w*\s)?class');
    RegExp regExJexp = new RegExp(r'\n\s*@jexp\s+((static\s+)?((\w+(<.*>)?)\s+(\w+)))');
    while (c < lines.length) {
      if (c < lines.length-2 && lines.substring(c, c + 2) == "//") {
        while (lines.substring(c, c + 1) != "\n") {
          c++;
        }
      }
      if (c < lines.length-2 && lines.substring(c, c + 2) == "/*") {
        while (lines.substring(c, c + 2) != "*/") {
          c++;
        }
      }
      if (state == ST_BEGIN) {
        if (lines.substring(c).startsWith(regExDecodable)) {
          state = ST_CLASS;
          classBegin = c;
        }
      }
      if (state == ST_CLASS) {
        if (lines.substring(c, c + 1) == "{") {
          brack++;
        }
        if (lines.substring(c, c + 1) == "}") {
          brack--;
          if (brack == 0) {
            classEnd = c;
            classes.add(new JsonClassData(regExClassName.firstMatch(lines.substring(classBegin, classEnd)).group(2), lines.substring(classBegin, classEnd), []..addAll(properties)));
            properties.clear();
            state = ST_BEGIN;
          }
        }
        if (brack == 1) {
          if (lines.substring(c).startsWith(regExJexp)) {
            properties.add(new JsonPropertyData()..name = regExJexp.matchAsPrefix(lines, c).group(6)..type = regExJexp.matchAsPrefix(lines, c).group(4));
          }
        }
      }
      c++;
    }
    return classes;
  }



  JsonDecoder(this.fn);

}