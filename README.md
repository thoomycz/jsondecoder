# JsonDecoder

JsonDecoder is a Dart library for classes to be able to deal with JSON.

This tool generates *.g.dart files from dart source code. Uses mirrors to analyze classes to generate required methods.
Mirrors are used only during generation, not runtime.

### Usage
Source class in classfile_data.dart
```dart
import 'package:jsondec_gen/annotation.dart';

class MyClass extends JsonDecodableMethod {
  int id;
  List<MyClass> children;
  Map<String, MyClass> parents;
  List<int> ints;
}
```

run command
```shell
jsondec-gen classfile_data.dart
```
import generated file
```dart
import "classfile_data.g.dart";

MyClass c = new MyClass().fromJsonString('{"id":1}');
print(c.toJsonString());

// OR

MyClass c = new MyClass().fromJson(JSON.decode(jsonString));
print(JSON.encode(c));
```