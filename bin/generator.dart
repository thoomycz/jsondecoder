#!/usr/bin/env dart

import 'dart:io';

main (List<String> args) {
  print("jsondec-gen 0.1.7");
  if (args.length == 1) {
    String file = args[0];
    String fn = file.split("/").last;
    Directory startDir = new Directory(file);
    Directory curDir = startDir;
    bool notFound = true;
    int c = 0;
    while (notFound) {
      c++;
      if (new File(curDir.absolute.path + "/pubspec.yaml").existsSync() == true) {
        notFound = false;
      } else {
        curDir = curDir.absolute.parent;
      };
      if (curDir.absolute.path == "/") {
        notFound = false;
        curDir = null;
      }
      if (c > 100) {
        notFound = false;
        curDir = null;
      }
    }
    if (curDir == null) {
      print("pubspec not foud");
    } else {
      var tmpDir = new Directory(curDir.absolute.path + "/tmp");

      if (tmpDir.existsSync() == false) {
        tmpDir.createSync();
      }

      String dirName = tmpDir.absolute.path.replaceAll('\\', '/');

      String abs = dirName + "/" + fn;

      String generatorFile = abs.replaceRange(
          abs.lastIndexOf(".dart"), abs.length, ".generator.dart");

      var srcFile = new File(file);

      var srcFileName = srcFile.absolute.path.replaceFirst(curDir.absolute.path, "");
      print(srcFileName);
      print(generatorFile);
      new File(generatorFile).writeAsStringSync(''' 
import 'package:jsondec_gen/class_generator.dart';
import '..$srcFileName';

main () {

  JsonDecoderGenerator a = new JsonDecoderGenerator("${srcFile.absolute.path}");
  a.run();

}
    ''');

      Process.run('dart', [generatorFile]).then((ProcessResult results) {
        print(results.stdout);
        print(results.stderr);
        new File(generatorFile).delete();
      });
    }
//    new JsonDecoder(args[0]).generate();
  } else {
    print("Enter file name");
  }
}