part of 'test_data.dart';

abstract class _$UzivatelMixin { 

  int ID;
  int b;
  dynamic c;
  Adresa adresa;
  Map<String, dynamic> mapa;
  Map<String, Adresa> adresy;
  List<Adresa> vylety;
  List<int> i;
  MaFromJson ma2;

  Uzivatel newInstance() {
    return new Uzivatel();
  }
  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      "ID": ID,
      "B": b,
      "C": c != null ? c.toJson() : null,
      "Adresa": adresa != null ? adresa.toJson() : null,
      "Mapa": mapa,
      "Adresy": adresy != null ? new Adresa().mapToJson(adresy) : null,
      "Vylety": vylety != null ? new Adresa().listToJson(vylety) : null,
      "I": i,
      "Ma2": ma2,
    };
  }
  Uzivatel fromJson(Map<String, dynamic> m) {
    if (m != null) {
      ID = m["ID"];
      b = m["B"];
      c = m["C"];
      adresa = new Adresa()..fromJson(m["Adresa"]);
      mapa = (m["Mapa"] as Map).map((k, v) { return new MapEntry(k, v as dynamic);});
      adresy = new Adresa().mapFromJson(m["Adresy"]);
      vylety = new Adresa().listFromJson(m["Vylety"]);
      i = (m["I"] as List)?.cast<int>()?.toList();
      ma2 = MaFromJson.fromJson(m["Ma2"]);
      return this;
    }
    return null;
  }

}

abstract class _$AdresaMixin { 

  String ulice;
  String mesto;
  String CasDoM;
  String Test;

  Adresa newInstance() {
    return new Adresa();
  }
  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      "Ulice": ulice,
      "Mesto": mesto,
      "CasDoM": CasDoM,
    };
  }
  Adresa fromJson(Map<String, dynamic> m) {
    if (m != null) {
      ulice = m["Ulice"];
      mesto = m["Mesto"];
      Test = m["Test"];
      return this;
    }
    return null;
  }

}

