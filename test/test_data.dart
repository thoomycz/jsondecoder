library json_decodable;

import 'package:jsondec_gen/annotation.dart';

part 'test_data_gen.dart';


class Uzivatel extends JsonDecodable<Uzivatel> with _$UzivatelMixin {
  int ID;
  int b;
  dynamic c;
  Adresa adresa;
  Map<String, dynamic> mapa;
  Map<String, Adresa> adresy;
  List<Adresa> vylety;
  List<int> i;
  MaFromJson ma2;
}

class Adresa extends JsonDecodable<Adresa> with _$AdresaMixin {
  String ulice;
  String mesto;
  String get CasDoM => ulice?.substring(3, 5);

  set Test(String s) {
  }
}

class MaFromJson {
  String value;

  static fromJson(String s) {

  }
}