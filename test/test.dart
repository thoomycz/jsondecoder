import 'test_data.dart';

main() {
  Uzivatel u = new Uzivatel()..ID = 1;
  u.vylety = [];
  u.mapa = {};
  u.mapa["b"] = "C";
  u.vylety.add(new Adresa()..mesto = 'Dst');
  u.i = [];
  u.i.add(10);
  u.i.add(12);

  u.adresa = new Adresa();
  u.adresa.mesto = "dobruska";

  u.adresy = {};
  u.adresy["test"] = new Adresa()..mesto = 'Praha';
  u.adresa.ulice = "testovaci ulice";

  String str = u.toJsonString();

  print(str);

  Uzivatel u2 = new Uzivatel();
  u2.fromJsonString(str);

  print(u2.toJsonString());

}